package com.example.experiment5;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button bt1,bt2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bt1=findViewById(R.id.ActivityShaSharedPreferences);
        bt2=findViewById(R.id.ActivitySQLite);
        bt1.setOnClickListener(this);
        bt2.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent=new Intent();
        switch (v.getId()){
            case R.id.ActivityShaSharedPreferences:
                intent.setClass(this,UseCount.class);
                break;
            case R.id.ActivitySQLite:
                intent.setClass(this,DBTest.class);
                break;
        }
        startActivity(intent);
    }
}
