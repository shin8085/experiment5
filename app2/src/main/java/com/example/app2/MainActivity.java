package com.example.app2;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Context useCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Context useCount = null;
        try{
            // 获取其他程序所对应的Context
            useCount = createPackageContext("com.example.experiment5",MODE_PRIVATE);
        }
        catch (PackageManager.NameNotFoundException e){
            e.printStackTrace();
        }
        // 使用其他程序的Context获取对应的SharedPreferences
        SharedPreferences prefs = useCount.getSharedPreferences("count",Context.MODE_PRIVATE);
        // 读取数据
        int count = prefs.getInt("count", 0);
        TextView show = findViewById(R.id.show);
        // 显示读取的数据内容
        show.setText("UseCount页面以前被打开了" + count + "次。");

    }
}
